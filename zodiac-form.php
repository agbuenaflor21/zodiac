<!DOCTYPE html>
<html>
<head>
	<title>Registration Form</title>
	<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/litera/bootstrap.css">
</head>
<body class="bg-primary">
	<h1 class="text-center text-white my-4">Welcome to Zodiac Checker</h1>
	<div class="col-lg-4 offset-lg-4">
		<form class="bg-light p-4" action="controllers/zodiac-process.php" method="POST">
			<div class="form-group">
				<label for="name">Name: </label>
				<input type="text" name="name" class="form-control">
			</div>
			<div class="form-group">
				<label for="birthMonth">Birth Month: </label>
				<input type="number" name="birthMonth" class="form-control">
			</div>
			<div class="form-group">
				<label for="birthday">Birth Day: </label>
				<input type="number" name="birthday" class="form-control">
			</div>
			<div class="text-center">
				<button type="submit" class="btn btn-success">Check Zodiac Sign</button>

			</div>
			<?php 
				session_start();
				session_destroy();
				if(isset($_SESSION['errorMsg'])){
			?>		
			<p>
				<?php echo $_SESSION['errorMsg']?>

			</p>
			<?php
				}
			?>


			
			
		</form>
	</div>

</body>
</html>